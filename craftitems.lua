local flowers_table = {name, desc, invimg, color
	{'black', 'Black', 'black', 'Black'}
	{'dark_grey', 'Dark Grey', 'dark_grey', 'Dark Grey'}
	{'grey', 'Grey', 'grey', 'Grey'}
	{'white', 'White', 'white', 'White'}
	{'brown', 'Brown', 'brown', 'Brown'}
	{'pink', 'Pink', 'pink', 'Pink'}
	{'red', 'Red', 'red', 'Red'}
	{'orange', 'Orange', 'orange', 'Orange'}
	{'yellow', 'Yellow', 'yellow', 'Yellow'}
	{'green', 'Green', 'green', 'Green'}
	{'dark_green', 'Dark Green', 'dark_green', 'Dark Green'}
	{'cyan', 'Cyan', 'cyan', 'Cyan'}
	{'blue', 'Blue', 'blue', 'Blue'}
	{'violet', 'Violet', 'violet', 'Violet'}
	{'magenta', 'Magenta', 'magenta', 'Magenta'}

}

for i in ipairs (flowers_table) do
		local name = flowers_table[i][1]
		local desc = flowers_table[i][2]
		local invimg = flowers_table[i][3]
		local color = flowers_table[i][4]

minetest.register_craftitem('flower_bushes:' ..name.. '_flowers', {
		description = (desc.. ' Flowers'),
		inventory_image = 'flower_bushes' ..invimg.. '_flowers.png',
		groups = {flower = 1, snappy = 3, attached_node = 1, flammable = 1, 'color_' ..color = 1},
	})

minetest.register_craft({
	output = 'dye:' ..name.. ' 1',
	recipe = {
		{'flower_bushes:' ..name.. '_flowers'}
		},
	})
end

end
