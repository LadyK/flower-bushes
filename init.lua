-- Bush Leaves
minetest.register_node('flower_bushes:bush_leaves', {
	description = ('Flower Bush Leaves'),
	drawtype = "allfaces_optional",
	waving = 1,
	tiles = {'flower_bushes_bush_leaves.png'},
	paramtype = "light",
	walkable = false,
	groups = {snappy = 3, flammable = 2, leaves = 1, hedge = 1},
	drop = {
		max_items = 1,
		items = {
			{items = {'flower_bushes:bush_leaves'}}
		}
	},
})

-- Flower Bushes
local flower_bushes_table = {name, desc, tiles}
	{'black', 'Black Flower Bush', 'black'}
	{'dark_grey', 'Dark Grey Flower Bush', 'dark_grey'}
	{'grey', 'Grey Flower Bush', 'grey'}
	{'white', 'White Flower Bush', 'white'}
	{'brown', 'Brown Flower Bush', 'brown'}
	{'pink', 'Pink Flower Bush', 'pink'}
	{'red', 'Red Flower Bush', 'red'}
	{'orange', 'Orange Flower Bush', 'orange'}
	{'yellow', 'Yellow Flower Bush', 'yellow'}
	{'green', 'Green Flower Bush', 'green'}
	{'dark_green', 'Dark Green Flower Bush', 'dark_green'}
	{'cyan', 'Cyan Flower Bush', 'cyan'}
	{'blue', 'Blue Flower Bush', 'blue'}
	{'violet', 'Violet Flower Bush', 'violet'}
	{'magenta', 'Magenta Flower Bush', 'magenta'}

minetest.register_node('flower_bushes:' ..name.. '_flower_bush', {
	description = desc,
	drawtype = 'allfaces_optional',
	waving = 1,
	tiles = {'flower_bushes_bush_leaves.png^(flower_bushes_shadows.png^flower_bushes_' ..tiles.. '_flowers.png)'},
	paramtype = 'light',
	walkable = false,
	groups = {snappy = 3, flammable = 2, leaves = 1, dig_immediate = 3, hedge = 1, not_in_creative_inventory = 0},
	drop = 'flower_bushes:' ..name.. '_flowers',
	drop = {
		max_items = 1,
		items = {
			{items = {'flower_bushes:' ..name.. '_bush_sapling'}, rarity = 5},
			{items = {'flower_bushes:' ..name.. '_flowers'}}
		}
	},
	sounds = default.node_sound_leaves_defaults(),
	node_dig_prediction = 'flower_bushes:bush_leaves',

	after_dig_node = function(pos, oldnode, oldmetadata, digger)
		minetest.set_node(pos, {name = 'flower_bushes:bush_leaves'})
		minetest.get_node_timer(pos):start(math.random(500, 2000))
	end,
})

-- Saplings
local saplings_table = {name, desc, tiles, invimg, weildimg
	{'black', 'Black Flower Bush Sapling', 'black', 'black', 'black'}
	{'dark_grey', 'Dark Grey Flower Bush Sapling', 'dark_grey', 'dark_grey', 'dark_grey'}
	{'grey', 'Grey Flower Bush Sapling', 'grey', 'grey', 'grey'}
	{'white', 'White Flower Bush Sapling', 'white', 'white', 'white'}
	{'brown', 'Brown Flower Bush Sapling', 'brown', 'brown', 'brown'}
	{'pink', 'Pink Flower Bush Sapling', 'pink', 'pink', 'pink'}
	{'red', 'Red Flower Bush Sapling', 'red', 'red', 'red'}
	{'orange', 'Orange Flower Bush Sapling', 'orange', 'orange', 'orange'}
	{'yellow', 'Yellow Flower Bush Sapling', 'yellow', 'yellow', 'yellow'}
	{'green', 'Green Flower Bush Sapling', 'green', 'green', 'green'}
	{'dark_green', 'Dark Green Flower Bush Sapling', 'dark_green', 'dark_green', 'dark_green'}
	{'cyan', 'Cyan Flower Bush Sapling', 'cyan', 'cyan', 'cyan'}
	{'blue', 'Blue Flower Bush Sapling', 'blue', 'blue', 'blue'}
	{'violet', 'Violet Flower Bush Sapling', 'violet', 'violet', 'violet'}
	{'magenta', 'Magenta Flower Bush Sapling', 'magenta', 'magenta', 'magenta'}

}

for i in ipairs (saplings_table) do
		local name = saplings_table[i][1]
		local desc = saplings_table[i][2]
		local tiles = saplings_table[i][3]
		local invimg = saplings_table[i][4]
		local weildimg = saplings_table[i][5]

minetest.register_node('flower_bushes:' ..name.. '_bush_sapling', {
		description = desc,
		drawtype = 'plantlike',
		waving = 1,
		tiles = {'flower_bushes_' ..tiles.. '_bush_sapling.png'},
		inventory_image = 'flower_bushes_' ..invimg.. '_bush_sapling.png',
		wield_image = 'flower_bushes_' ..weildimg.. '_bush_sapling.png',
		groups = {snappy = 2, dig_immediate = 3, flammable = 2, attached_node = 1, sapling = 1},
		sunlight_propagates = true,
		paramtype = 'light',
		walkable = false,
		on_timer = grow_sapling,
		selection_box = {
			type = 'fixed',
			fixed = {-0.25, -0.5, -0.25, 0.25, 0.875, 0.25},
			},
		sounds = default.node_sound_leaves_defaults(),

	on_construct = function(pos)
		minetest.get_node_timer(pos):start(math.random(500, 2000))
	end,

	on_place = function(itemstack, placer, pointed_thing)
		itemstack = default.sapling_on_place(itemstack, placer, pointed_thing,
			'flower_bushes:' ..name.. '_bush_sapling',
			-- minp, maxp to be checked, relative to sapling pos
			{x = -1, y = 0, z = -1},
			{x = 1, y = 1, z = 1},
			-- maximum interval of interior volume check
			2)

			return itemstack
		end,
	})
	end
