- Flower Bushes
Flower Bushes mod for Minetest by LadyK.

-- Description
This mod adds flowering bushes to the game. Something that I feel has been lacking.

-- Depends
default
dyes?
hedges?
ethereal?
technic?

-- If using Flower Bushes with Shara's Hedges mod, add this definition to her mod's file:

```
hedges.register_hedge("mod_name:hedge_name", {
	texture = "texture.png",
	material = "mod_name:material_name",	
})

```

--  Credits
Special Thanks to:
* Agura (aka xXAguraXx) for the inspiration to create this mod.
* CalebJ, GreenDimond, and NathanS for giving me modding advice.
* Shara RedCat for Hedges mod, that contributed to the idea of how this mod could be used.
* The creator of the default:blueberry_bush which also contributed to the idea of how this mod works and should be used.
* Rubenwardy for the Minetest Modding Book.
* kristovish for making a working version on Moonlight Falls and for encouraging me to finish creating the textures.
* A lot of other modders whose codes I looked at to compare and see how things were written.

-- Links
* Hedges Mod on Minetest Forum(https://forum.minetest.net/viewtopic.php?t=20427) or GitHub (https://github.com/Ezhh/hedges)
* Minetest Modding Book (https://rubenwardy.com/minetest_modding_book/en/index.html)

-- Want to see how this mod could look and work on your server? Join me on Moonlight Falls and see a working customized version of this mod.

-- As the mod is written now, it will give you errors, however if you can code, feel free to modify the code to whatever you want this mod to do for your server. (If you do, I would love to know what server it's being used on, so I can come see!)
